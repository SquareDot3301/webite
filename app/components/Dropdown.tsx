"use client";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import {
  customFetch,
  displayError,
  displaySuccess,
  getError,
  isConnected,
} from "../utils/functions_utils";
import ModalPostCreation from "./ModalPostCreation";

const Dropdown = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const closeDropdown = () => {
    setIsOpen(false);
  };

  const [errorTimer, setErrorTimer] = useState<
    ReturnType<typeof setTimeout> | undefined
  >(undefined);
  const [error, setError] = useState<string>("");

  const [infos, setInfos] = useState<User | null>(null);

  async function getInfos() {
    const res = await customFetch(
      `https://quetzer.creativeblogger.org/@me/`,
      "GET"
    );

    if (!res.ok) {
      displayError(
        getError(await res.json()),
        setError,
        errorTimer,
        setErrorTimer
      );
      return;
    }

    setInfos(await res.json());
  }

  function delete_cookie() {
    document.cookie = "token" + "=; expires=Thu, 01-Jan-70 00:00:01 GMT;";
  }

  async function logout() {
    const res = await customFetch(
      `https://quetzer.creativeblogger.org/auth/logout`,
      "GET"
    );
    delete_cookie();

    if (!res.ok) {
      displayError(
        getError(await res.json()),
        setError,
        errorTimer,
        setErrorTimer
      );
      return;
    }
    displaySuccess(
      "Déconnecté avec succès !",
      setError,
      errorTimer,
      setErrorTimer
    );
    location.assign("/");
  }

  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    async function fetchData() {
      await getInfos();
    }
    setMounted(true);
    if (isConnected() === true) {
      fetchData();
    }

    return () => {};
  }, []);

  const [modalState, setModalState] = useState(false);

  return (
    <div className="">
      {modalState && <ModalPostCreation setModalState={setModalState} />}
      <div className="relative inline-block">
        <div className="flex items-center">
          <button
            onClick={() => {
              closeDropdown();
              setModalState(true);
            }}
          >
            <Image
              src={"/new.svg"}
              alt="Create new story"
              height={65}
              width={65}
            />
          </button>
          <button
            type="button"
            className="px-4 py-2 text-white font-medium rounded-lg"
            onClick={toggleDropdown}
          >
            <Image src={"/user.svg"} alt="User image" height={80} width={80} />
          </button>
        </div>

        {isOpen && (
          <div className="origin-top-right absolute right-0 mt-2 w-44 rounded-lg shadow-lg bg-white ring-1 ring-black ring-opacity-5">
            <ul
              role="menu"
              aria-orientation="vertical"
              aria-labelledby="options-menu"
            >
              <li>
                <p className="block px-4 py-2 text-sm text-black">
                  Hello {infos?.username} !
                </p>
              </li>
              <li>
                <Link
                  href="#"
                  className="block px-4 py-2 text-sm text-black hover:bg-gray rounded-lg"
                  onClick={closeDropdown}
                >
                  Profile
                </Link>
              </li>
              <li>
                <Link
                  href="#"
                  className="block px-4 py-2 text-sm text-black hover:bg-gray rounded-lg"
                  onClick={closeDropdown}
                >
                  Settings
                </Link>
              </li>
              <li>
                <Link
                  href="#"
                  className="block px-4 py-2 text-sm text-black hover:bg-red-200 rounded-lg"
                  onClick={() => {
                    closeDropdown;
                    logout();
                  }}
                >
                  Logout
                </Link>
              </li>
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};

export default Dropdown;
