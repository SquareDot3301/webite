import { FormEvent, TextareaHTMLAttributes, useState } from "react";
import {
  customFetch,
  displayError,
  displaySuccess,
  getError,
} from "../utils/functions_utils";
import Image from "next/image";

const Modal = (props: any) => {
  const [errorTimer, setErrorTimer] = useState<
    ReturnType<typeof setTimeout> | undefined
  >(undefined);
  const [error, setError] = useState<string>("");

  const [isTagModalOpen, setTagModalOpen] = useState(false);

  const [content, setContent] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setContent(event.target.value);
  };

  async function onPostSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    // const content = (document.getElementById("content") as HTMLTextAreaElement)
    //   .value;
    // const tags = (document.getElementById("tags") as HTMLInputElement).value;
    // const image = (document.getElementById("image") as HTMLTextAreaElement)
    //   .value;

    // const res = await customFetch(
    //   "https://quetzer.creativeblogger.org/posts",
    //   "POST",
    //   JSON.stringify({
    //     content: content,
    //     tags: tags,
    //     image: image,
    //   })
    // );

    // if (!res.ok) {
    //   displayError(
    //     getError(await res.json()),
    //     setError,
    //     errorTimer,
    //     setErrorTimer
    //   );
    //   return;
    // }

    displaySuccess(
      "Post publié avec succès !",
      setError,
      errorTimer,
      setErrorTimer
    );
    console.log("Post publié !");
  }
  return (
    <>
      <div className="fixed inset-0 opacity-25 bg-black"></div>
      <div className="fixed inset-0 flex justify-center items-center z-10">
        <div className="bg-slate-900 text-white rounded-3xl w-11/12 sm:w-4/5 lg:w-2/3 xl:w-1/2 2xl:w-1/3 h-2/3">
          <div className="flex justify-end">
            <button
              className="hover:bg-red-100 rounded-3xl py-3 px-4 m-3"
              onClick={() => props.setModalState(false)}
            >
              <Image src={"/cross.svg"} alt="Close" height={20} width={20} />
            </button>
          </div>
          <h2 className="text-center text-4xl pb-8">Talk about you !</h2>
          <div>
            <form action="" onSubmit={onPostSubmit} className="">
              {isTagModalOpen === false && (
                <div className="flex items-center justify-center">
                  <textarea
                    name="content"
                    className="textarea bg-transparent border-2 border-white rounded-2xl p-2 text-xl w-11/12 flex justify-center focus:outline-none h-80"
                    placeholder="Today, my day was..."
                    id="content"
                    value={content}
                    onChange={handleChange}
                  />
                </div>
              )}
              {isTagModalOpen && (
                <div className="flex items-center justify-center">
                  <input
                    type="text"
                    name="tags"
                    id="tags"
                    className="border-2 border-slate-800 bg-transparent rounded-3xl w-1/2 px-3 py-2 mx-3"
                  />
                  <button
                    className="p-2 bg-red-200 rounded-3xl duration-150 hover:bg-red-100 mx-2 md:mx-5"
                    onClick={() => setTagModalOpen(false)}
                  >
                    Close
                  </button>
                  <button className="p-2 bg-primary rounded-3xl duration-150 hover:bg-hover hover:text-black">
                    Done
                  </button>
                </div>
              )}
              {isTagModalOpen === false && (
                <div className="flex justify-between items-center my-10">
                  <div className="ml-9 flex">
                    <label htmlFor="image" className="relative cursor-pointer">
                      <div className="h-12 w-12">
                        <Image
                          src="/image.svg"
                          alt="File"
                          className="h-full w-full object-cover"
                          height={40}
                          width={40}
                        />
                      </div>
                      <input id="image" type="file" className="hidden" />
                    </label>
                    <button onClick={() => setTagModalOpen(true)}>
                      <Image
                        src={"/hashtags.svg"}
                        alt="Add tags"
                        height={45}
                        width={45}
                        className="ml-5"
                      />
                    </button>
                  </div>
                  <div className="mr-9">
                    <button
                      className="bg-primary text-white p-3 rounded-2xl duration-150 hover:bg-hover hover:text-black"
                      type="submit"
                    >
                      Koyer !
                    </button>
                  </div>
                </div>
              )}
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modal;
