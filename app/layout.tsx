import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";

export const metadata: Metadata = {
  title: "Quetzer - Home",
  description: "Welcome to Quetzer !",
  icons: {
    icon: "/quetzer-hello.png",
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body suppressHydrationWarning className="bg-slate-800">
        {children}
      </body>
    </html>
  );
}
