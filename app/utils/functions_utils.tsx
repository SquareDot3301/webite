import { useState } from "react";

function getHumanDate(date: string) {
  const parsed_date = new Date(Date.parse(date));

  return `${parsed_date.toLocaleDateString()} à ${parsed_date.toLocaleTimeString()}`;
}

function getToken() {
  if (typeof document !== "undefined") {
    let cookies = document.cookie;
    let token = cookies.split("; ").find((e) => e.startsWith("token="));
    if (token === undefined) {
      return "";
    }
    return token.split("=")[1];
  }
  return "";
}

function isConnected() {
  return getToken().length != 0;
}

function getCookie(name: any) {
  return document.cookie.split(";").some((c) => {
    return c.trim().startsWith(name + "=");
  });
}

function getError(error: any) {
  return error.errors[0].message;
}

async function customFetch(
  url: string,
  method: string = "GET",
  body?: BodyInit,
  json: boolean = true
) {
  let headers = {};
  if (json) {
    headers = {
      Authorization: `Bearer ${getToken()}`,
      "Content-Type": "application/json",
    };
  } else {
    headers = {
      Authorization: `Bearer ${getToken()}`,
    };
  }

  return await fetch(url, {
    method: method,
    body: body,
    headers: headers,
  });
}
export function displayError(
  errorText: string,
  setError: React.Dispatch<React.SetStateAction<string>>,
  errorTimer: ReturnType<typeof setTimeout> | undefined,
  setErrorTimer: React.Dispatch<
    React.SetStateAction<ReturnType<typeof setTimeout> | undefined>
  >
) {
  setError(errorText);
  if (errorTimer) {
    clearTimeout(errorTimer);
  }
  setErrorTimer(
    setTimeout(() => {
      setError("");
    }, 2000)
  );
}

export function displaySuccess(
  successText: string,
  setSuccess: React.Dispatch<React.SetStateAction<string>>,
  successTimer: ReturnType<typeof setTimeout> | undefined,
  setSuccessTimer: React.Dispatch<
    React.SetStateAction<ReturnType<typeof setTimeout> | undefined>
  >
) {
  setSuccess(successText);
  if (successTimer) {
    clearTimeout(successTimer);
  }
  setSuccessTimer(
    setTimeout(() => {
      setSuccess("");
    }, 2000)
  );
}

function findPermissions(permission: number) {
  if (permission === -1) {
    return "suspendu";
  } else if (permission === 0) {
    return "membre";
  } else if (permission === 1) {
    return "modérateur";
  } else {
    return "admin";
  }
}

export {
  getHumanDate,
  getToken,
  isConnected,
  getError,
  customFetch,
  getCookie,
  findPermissions,
};
