"use client";
import { useEffect, useState } from "react";
import { Navbar } from "./components/Navbar";
import {
  customFetch,
  displayError,
  getError,
  isConnected,
} from "./utils/functions_utils";
import Image from "next/image";

export default function Home() {
  const [errorTimer, setErrorTimer] = useState<
    ReturnType<typeof setTimeout> | undefined
  >(undefined);
  const [error, setError] = useState<string>("");

  const [infos, setInfos] = useState<User | null>(null);

  async function getInfos() {
    const res = await customFetch(
      `https://quetzer.creativeblogger.org/@me/`,
      "GET"
    );

    if (!res.ok) {
      displayError(
        getError(await res.json()),
        setError,
        errorTimer,
        setErrorTimer
      );
      return;
    }

    setInfos(await res.json());
  }

  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    async function fetchData() {
      await getInfos();
    }
    setMounted(true);
    // fetchData();
    return () => {};
  }, []);

  return (
    mounted && (
      <div className="bg-slate-800">
        <main className="flex items-center justify-center h-screen">
          <button className="hidden lg:block lg:pl-2 xl:pl-5">
            <Image
              src={"/arrow-left.svg"}
              alt="Arrow left"
              height={150}
              width={150}
            />
          </button>
          <div className="w-11/12 sm:w-4/5 lg:w-3/4 max-w-screen-lg h-full p-8 bg-slate-900">
            <div className="h-min">
              <Navbar />
            </div>
            <div className="h-5/6 bg-red-100">
              <div className="h-5/6 bg-primary"></div>
            </div>
          </div>
          <button className="hidden lg:block lg:pl-2 xl:pl-5">
            <Image
              src={"/arrow-right.svg"}
              alt="Arrow right"
              height={150}
              width={150}
            />
          </button>
        </main>
      </div>
    )
  );
}
